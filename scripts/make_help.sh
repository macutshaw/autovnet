#!/bin/bash
if [ ! -f setup.py ]; then
    echo "Run from root of checkout"
    exit 1
fi
# pip install typer-cli
if command -v typer; then
    typer autovnet.main utils docs --name autovnet --output docs/usage/help.md
else
    echo "[!] 'pip install typer-cli' to regenerate help.md"
fi
sed -i "s|${HOME}|~|g" docs/usage/help.md
