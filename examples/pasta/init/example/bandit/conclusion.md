# Conclusion

While this definitely was not a helpful walkthrough of [bandit](https://overthewire.org/wargames/bandit/),
it hopefully is a helpful example showing how you can structure educational / lab-style content
using [`autovnet pasta`](https://autovtools.gitlab.io/autovrtfm/autovnet-docs/usage/rtfm/pasta)!
