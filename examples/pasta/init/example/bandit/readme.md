# Bandit

As a practical example of what you can do with `pasta`, here is a quick exercise that is a walkthrough of
[Over The Wire: Bandit](https://overthewire.org/wargames/bandit/),
a fun, gamified introduction to the basics of Linux and CTFs


## Prereqs

### System

This guide assumes a working Linux installation with basic tools like `ssh`, `ping`, `curl`, etc. installed.

### Networking

You need internet access, and the ability to connect out via TCP to random high ports, e.g. `2220`

* Some firewalls block this (e.g campus or corporate networks)

  - Using a VPN can often bypass this blocking
