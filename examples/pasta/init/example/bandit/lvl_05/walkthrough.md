# Level 5

## Connecting

```{: .host .host-title}
ssh -oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null -p 2220 bandit5@bandit.labs.overthewire.org
```

```{: .check .check-title .no-copy}
...
bandit5@bandit.labs.overthewire.org's password:
```

Enter the password:
```{: .target .target-title}
lrIWWI6bB37kxfiCQZqUdOIYfr6eEeqR
```

You should successfully log in.
```{: .check .check-title .no-copy}
...
bandit5@bandit:~$
```

## Winning

```{: .target .target-title}
find inhere/ -type f -exec wc -c {} \; | grep 1033
```

```{: .check .check-title .no-copy}
1033 inhere/maybehere07/.file2
```

```{: .target .target-title}
cat inhere/maybehere07/.file2
```

```{: .check .check-title .no-copy}
P4L4vucdmLnm8I7Vl7jG1ApGSfjYKqJU



```
