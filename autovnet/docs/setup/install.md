# Install

[TOC]

## Overview

`autovnet` contains a server component and a client component.

* The `autovnet server` should run in a central location reachable by all Blue / Red systems
* The `autovnet clients` run on Red systems and allow Red Teamers to control the `autovnet server`
* No modifications are needed to any Blue systems
* The Network Administrator should add a static firewall route such that all simulated networks are routed to the `autovnet server`
    - This is the 'trick' that makes `autovnet` work in an exercise environment - the Network Administrator is the default gateway (effective 'ISP'), and can send packets where they please

## Requirements

### _autovnet server_

Tested on Ubuntu 22.04 LTS and Kali 2023.3.
Yes, you can get it working on other distros, but YMMV and there won't be good docs.
The installer currenly only supports `apt`, so `apt`-based distros are more likely to work with less friction.

#### Logical Requirements

**You do not need to manually install anything if you are going to use [the easy install](#easy-install), which you should**

* Recent Linux with standard utilities (e.g. `sudo`, `ip`, `ssh`, `curl`)
* `Python >= 3.9` (required by some dependencies)
    - Recent Python 3 (e.g. >= 3.7) is fine for most features
* Your system must support a reasonable version of `docker` (the easy installer will install it for you)
* Root privs
    - Specifically, a non-root user with `sudo` access
* Outbound Internet access (at least to install and start the `autovnet server`)
    - Eventual support for easy installation in air-gapped networks is planned
* The ability / permission to route an arbitrary network block to this system
    - The recommended way is to add a static firewall route to a firewall that sees all outbound Red / Blue traffic.
* Blue <-> `autovnet server` and Red <-> `autovnet server` traffic should be unfiltered
    - Obviously, your firewall rules must accomodate getting the packets to / from the `autovnet server`

### _autovnet client_

Tested on Kali 2023.3.
`autovnet` attempts to maintain compability with the "latest" release of Kali.

If the latest official release of Kali does not, open an issue and/or PR to fix.

## Diagram

For reference, this is the network topology we are aiming for:
![deployment.svg](../img/deployment.svg)

## Easy Instructions

These instructions will install the `autovnet` program.

The `autovnet server` (part of the Red Team infrastructure) and all `autovnet clients` (the Red Team player machines) need to install `autovnet`.

### Easy Install

If running a supported platform, the easy install script is **highly recommended**.

Current supported platforms (other versions likely work, but are not verified):

* Ubuntu 22.04
* Kali 2023.3

If the easy install script doesn't work on your platform, you may need to trace through exactly what it does and manually perform similar setup tasks.

**As a regular user (not root):**
```bash
curl -s https://gitlab.com/autovtools/autovrtfm/autovnet/-/raw/main/ansible/bootstrap.sh | bash
```

You'll get a password prompt from `sudo`, then eventually a prompt from `ansible` asking for the `BECOME` password.
Enter the password you use with `sudo` for both.

After both passwords have been entered, no additional input is required - it's going to take a while.

If all goes well, your system will reboot.

> If you got an error, re-running the bootstrap script might fix it.
> You may need to manually reboot afterwards.

After the reboot, log in and test your install with:
```bash
~/autovnet/scripts/test_install.sh
```

#### Permission Denied

**You need to be a member of the `docker` group. The installer should do this for you**

If you get 'Permission Denied' from `docker` (e.g. `docker ps`):

1. Reboot and try again
2. If you _still_ get permission denied:

    * As your regular user:
```bash
sudo usermod -aG docker "${USER}"
sudo reboot
```

### Easy Update

#### Users

Once you have installed `autovnet`, you can easily update with an included helper script (already added to your `PATH` for your convenience)

As the user you used to install autovnet:
```bash
.autovnet-update
```

#### Server

To update an `autovnet server` in place:

```bash
# Stop and remove the autovnet service
sudo systemctl stop autovnet.service
autovnet rtfm server uninstall

# You can switch to an alternate stream here, if desired
.autovnet-update

# Reinstall the autovnet server
autovnet rtfm server install
```

### Alternate Streams

#### `test` stream

If you want to get the latest (unstable) features as soon as they might work, you can switch to the `test` branch.

If you already have `autovnet` installed:
```bash
.autovnet-switch test
```

Or, if you are installing for the first time:
```bash
curl -s https://gitlab.com/autovtools/autovrtfm/autovnet/-/raw/test/ansible/bootstrap.sh | AUTOVNET_INSTALL_BRANCH=test bash
```

Then, each call to `.autovnet-update` will pull updates from the `test` branch.

You can switch back to `main` later with
```bash
.autovnet-switch
```

#### `dev` stream

There is also a `dev` branch, but it will contain work-in-progress commits that may prevent `autovnet` from functioning at all.

It's the bleeding edge, and most users are better off using `test` for personal testing and `main` (or even a [pinned release](#pinned-releases))
for use on game day.

If you already have `autovnet` installed:
```bash
.autovnet-switch dev
```

Or, if you are installing for the first time:
```bash
curl -s https://gitlab.com/autovtools/autovrtfm/autovnet/-/raw/dev/ansible/bootstrap.sh | AUTOVNET_INSTALL_BRANCH=dev bash
```

Then, each call to `.autovnet-update` will pull updates from the `dev` branch.

You can switch back to `main` later with
```bash
.autovnet-switch
```

#### Pinned Releases

In the (hopefully) unlikely event that a release introduces a major regression that you cannot wait to be fixed, you can substitue a tag name
(e.g. `v1.2.3`) for the branch name to force a revert to a previous, pinned version.

#### Installing `autovnet` from a Fork

If you happen to maintain your own fork of `autovnet`, you can alter the URLs that `autovnet`'s installer uses.

If your repo was hosted on gitlab and and your account was named `FORK`, the syntax would look something like this
```
curl -s https://gitlab.com/FORK/autovrtfm/autovnet/-/raw/main/ansible/bootstrap.sh | BOOTSTRAP_YML_URL="https://gitlab.com/FORK/autovnet/-/raw/main/ansible/bootstrap.yml" AUTOVNET_GIT_URL="https://gitlab.com/FORK/autovnet.git" bash
```

This can be useful if you need to install `autovnet` without direct access to `gitlab.com` (although any number of other installation steps may fail if you don't have interet access).,
or if you want to test your change to the installer before submitting a PR.

## Manual Install

Manual installation is not documented or officially supported.
[The easy install](#easy-install) tries really hard to get all the dependencies just right.
If you can't use the easy installer and / or some subset of the dependencies,
you'll have to trace through the easy installer and replicate the setup steps that matter to you.

PRs welcome for installer improvements to cover more systems!

