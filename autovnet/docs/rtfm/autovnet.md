# autovnet

[TOC]

## `autovnet` Basics

1. Pick network block(s) that are not in use to simulate (e.g. `10.137.0.0/16`)
    * Pretend this block belongs to a cloud provider. It's the wild west, so no traffic to or from these IPs are inherently good or bad 


2. Ask the Cyber Range network administrator very nicely to: 
    * Provision *1* central VM (the `autovnet server`) that *all* Blue Teams and *all* Red Teams can reach
    * Add *1* static firewall route at the competition firewall level that routes *all* traffic to simulated networks (e.g. `10.192.0.0/10`) to the `autovnet server`
    * [Configure DNS](#dns) using one of the recommended strategies, so that `autovnet` can respond to special in-game DNS queries


3. Configure and start the `autovnet server`.
    * It will now respond to all IPs in the simulated network(s)


4. Install `autovnet` (client) with the server's configuration / keys / etc. on all Red Team machines (super easy)


5. Red Teamers run `autovnet` commands and get instant (opt-in) access to randomized IP obfuscation / redirection that passes the acid tests with flying colors


## Under the Hood

`autovnet` uses quantum tunneling to achieve _Spooky Networking at a Distance_ (`SNaaD`) ... or something.

But really, when you run an `autovnet` command, a lot of magic happens to achieve the end result.
While the source itself will always be the definitive source of truth for how things work, this should help demystify the moving pieces and the underlying techniques used.


### Core: IP Simulation

At its core, `autovnet` very efficiently "simluates" many, many devices controlled by `autovnet` players (Red Team).
In a standard deployment, a single `autovnet` server will receive and produce IP traffic from _millions_ of unique IP addresses.

This is achieved by:

1. Configuring a route on the exercise network to make sure all traffic to / from the "simluated" network is routed to the `autovnet server`

2. Using the [Linux AnyIP](https://blog.cloudflare.com/how-we-built-spectrum/) trick to cause the `autovnet server` to adopt / respond to all traffic in the simluated network.

The `AnyIP` trick does the heavy lifting here.
It's built in to a vanilla Linux Kernel, and is the underlying technology for why all of `127.0.0.0/8` works like `127.0.0.1` (`localhost`).


### Topology

Here is an example network topology incorporating `autovnet`:

![deployment.svg](../img/deployment.svg)

#### To `NAT` or not to `NAT`

Some networks choose to [NAT all outbound traffic from Red Team](paradigm.md#n-1-red-teaming-nat).
`autovnet` should still work as expected, as long as the `autovnet` server's outbound connections are _not_ subject to the `NAT`.
So you can `NAT` if you really want to, but `autovnet` supercedes any advatange you'd get from `NAT`.

#### Example: Route Configuration

The most important part of your network configuration is the static route that bends traffic destined for the `autovnet` simluation block to the `autovnet` server, where it can be processed.

https://docs.netgate.com/pfsense/en/latest/routing/static.html

#### DNS

To use `autovnet`'s awesome `dns` features, you need to incorporate `autovnet` into your `dns` setup.

There are mutliple ways to do this in a functional way.
This guide includes example configurations.

##### Example: Domain Override

Part of your `autovnet` game config is a made-up [TLD](https://en.wikipedia.org/wiki/List_of_Internet_top-level_domains) that in-game `dns` registrations will use.
(This approach allows you to allow Red Team to freely register domains without accidentally or intentionally _breaking the internet_ for themselves and Blue Team.)

* For example, if `.avn` is your `tld`, then Red Team can register domains like `foo.avn`, `foo.bar.avn`, etc.

The `autovnet` server is the authority for the `.avn` domain, so we need to configure `dns` so that queries for `*.avn` domains are sent to the `autovnet` server.

The recommended topolgy is shown below.

Here, there is an existing `dns` server (the game firewall) that is set as the primary `dns` server for all `blue` and `red` systems.

* Configure the existing `dns` server to route _only_ queries for the game TLD (e.g. `*.avn`) to the `autovnet` server
* This reduces server load and prevents internet distruption if the `autovnet` server is ever unavailable, since only `*.avn` queries are handled

![dns_override.svg](../img/dns_override.svg)

You (the network admin) need to figure out how to set this up in your network. These examples may help:

* [Example: `pfSense` Domain Override](https://docs.netgate.com/pfsense/en/latest/services/dns/forwarder-overrides.html#domain-overrides)
* [Example: `dnsmasq`](https://stackoverflow.com/questions/22313142/wildcard-subdomains-with-dnsmasq)

##### Example: Primary DNS

If you are unable to configure the existing `dns` server, or the recommended topology above does not work for you, `autovnet` will function as a primary `dns` server, if needed.

* Set the `autovnet` server as the primary `dns` server for all Blue and Red systems
    - If using `DHCP` or a `VPN` for Blue / Red networks, update the configuration to use `autovnet` as the `dns` server
* Set the existing `dns` server as the secondary `dns` server for all Blue and Red systems (in case the `autovnet` server is ever unavailable)
* Caution: the `autovnet` server should use the `existing` dns server (or a public `dns` server like `1.1.1.1`, `8.8.8.8`, `9.9.9.9`, etc.) as its primary `dns`.
    - avoid a `dns` loop - the `autovnet` server needs a valid "upstream" `dns` server it can forward non-game queries to

![dns_primary.svg](../img/dns_primary.svg)

### Listen: Reverse / Callback

The `listen` primitive is based on [`SSH` Remote Port Forwarding](https://www.ssh.com/academy/ssh/tunneling/example).
The `autovnet server` runs an `SSH` server configured with `GatewayPorts clientspecified`, which lets the `SSH` client (`autovnet` player) bind to any `IP` address assigned to the `autovnet server`.

Because of the `AnyIP` trick at the core of `autovnet`, players can bind to any of the `IP`s simulated by `autovnet`, because `SSH` considers them all valid addresses.

When you (the player) create a new `listen`er, the `autovnet client` (your machine) randomly chooses an `IP` address, and creates a `ssh -R RANDOM_IP:REMOTE_PORT:LOCAL_IP:LOCAL_PORT ...` tunnel.

When targets connect to `RANDOM_IP:REMOTE_PORT`, the connection is proxied locally to your machine at `LOCAL_IP:LOCAL_PORT`.

This feature is secured by the `SSH` protocol: to create a `listen`er, you must have the `autovnet` player `SSH` key, which is securely distributed to players only as part of the `autovnet` game configuration.

#### Diagram

Here is an example of an obfuscated Blue -> Red callback, using the `listen` primitive

![listen.svg](../img/listen.svg)


### Proxy: Forward / Proxy


#### Diagram

Here is an example of an obfuscated Red -> Blue connection

![proxy.svg](../img/proxy.svg)


### Tun: `1:1` IP Mapping

The `tun` feature of `autovnet` is one of its most complex.

It allows:

* Delivery of arbitrary `IP` packets, through the use of a local [TUN interface](https://en.wikipedia.org/wiki/TUN/TAP) (like a [VPN](https://en.wikipedia.org/wiki/Virtual_private_network) provides.)
* Remapping the remote end of the `tun` to an abitrary `IP` in the `autovnet` `IP` pool
    - This means that the `IP`s given by `tun` span the entire `autovnet` pool, and therefore also scale up to the entire `IP` space

The `tun` feature is powered by:

* [`nebula`](https://en.wikipedia.org/wiki/Virtual_private_network), which we use to create an [overlay network](https://en.wikipedia.org/wiki/Overlay_network)
* A custom [IPAM](https://en.wikipedia.org/wiki/IP_address_management) API, used to securely issue `nebula` client configurations to players, so they can join the overlay network
* [`iptables`](https://linux.die.net/man/8/iptables) on the `autovnet` server, and a custom API that manages `IP` mappings by creating [SNAT](https://www.linuxtopia.org/Linux_Firewall_iptables/x4658.html) and [DNAT](https://www.linuxtopia.org/Linux_Firewall_iptables/x4013.html) rules
* Some `ip route`s and `ip rule`s on the client / `autovnet` player system to ensure traffic that should be routed through the `tun` are actually routed through the `tun`

The `tun` commands provided by `autovnet` do everything for you, so everything [just works](../usage/rdn/tun.md).

You might be thinking:

> `iptables`? [You said that was bad!](README.md)

`iptables` is a bad solution for `IP` simulation itself (e.g. creating billions of `iptables` rules, 1 for each `IP` to simluate) - that is an `O(simulation_size)` solution (very big number).
But it is a great solution in the context of `tun`.

`tun` requires `O(num_active_tuns)`-many `iptables` rules, and `O(num_active_tuns) ~= O(num_players)` (managable number).
But the `IP` that it gives out are from the full, `O(simlutation_size)` `IP` pool!

Additionally, other _bad_ uses of `iptables` fail when players are behind a `NAT`, or using a VM that is on a `NAT` network will fail (e.g. redirecting packets from a pivot box outside of the `NAT` will fail).
The use of the overlay network solves this - all players (which already can connect to the `autovnet` server) become directly reachable (1 logical hop, same subnet) when they join the `tun` network.
So even if the player system is behind multiple `NAT`s, the `iptables` rules that are created will work as expected.

Other bad ideas include custom web GUIs for creating `iptables` rules - it's error prone and tedious.
`autovnet` intentionally does not expose full `iptables` functionality to players - it safely creates exactly the `iptables` rules you need, exactly when you need them.

So `autovnet`'s `tun` feature the best of all the worlds worlds; billions of `IP`s, at the cost of a very small number of simple `iptables` rules,
that are programatically created and deleted for you under the hood.


#### Diagram

Here is a quick diagram showing `tun` in action.

![tun.svg](../img/tun.svg)
