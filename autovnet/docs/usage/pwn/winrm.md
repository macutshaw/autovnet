# WinRM

[TOC]

## `.proxy-winrm`

If your Windows targets are set up for WinRM (if you are a Wargame facilitator, **you should make this so**), you can use `.proxy-winrm`,
which is a wrapper around the popular [evil-winrm](https://github.com/Hackplayers/evil-winrm)

```bash
$ .proxy-winrm -u victim -i target -p 'Passw0rd!'

Evil-WinRM shell v3.3

Info: Establishing connection to remote endpoint

*Evil-WinRM* PS C:\Users\victim\Documents>
```

The result is ssh-like remote access to `powershell`, and `netstat` will show a random, proxied source IP for the connection.
