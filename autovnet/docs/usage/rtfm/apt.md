# Advanced Persistent Thread (APT) Emulation

[TOC]

## Intro

Real-world [APTs](https://csrc.nist.gov/topics/security-and-privacy/risk-management/threats/advanced-persistent-threats) use infrastructure and [TTPs](https://csrc.nist.gov/glossary/term/Tactics_Techniques_and_Procedures).
Defenders and Cyber Threat Analysts identify these things, and attempt to coorelate related [IoCs](https://csrc.nist.gov/glossary/term/ioc)
and attribute them to a particular actor.

Each APT may have their own unique TTPs, goals, or targets.
APTs can be related to each other (perhaps backed by the same entity). These connections are often revealed when IoCs / TTPs are observed to overlap between actors.

`autovnet` allows you to roleplay as an APT, with a particular set of TTPs and infrastructure (IP addresses, `dns` names, etc.)

By default, `autovnet` commands roll random infrastructure from the default infrastructure pool (the giant block of IP addresses assigned to the `autovnet server` as part of the game config).

To roleplay as an APT, `autovnet` allows you to partion a subset of the available infrastructure to serve as your APT's infrastructure.
This collection of infrastructure (IP blocks, `dns` names, etc.) is refered to as an `APT profile`.

When an APT profile is active, `autovnet` commands that you run will draw IP addresses from the pool associated with your APT (as stored in your APT profile).

## Config Generation

Some APT information is generated as part of the `autovnet` game configuration, and distributed to all players as part of the game config.

When the game config is generated on the `autovnet` server, the entire IP pool is partitioned into `regions` and `APT Families`, described below.

  * This is done in large CIDR blocks, so the resulting data is reasonably sized

This means that all IPs in the `autovnet` pool can be categorized with a paricular `region` and `APT Family`.

  * `UNKNOWN` is treated as a special value

## Regions

Each IP address in the `autovnet` pool belongs to a `region`.
The real-world analog is a geo-locatable area, like `France`, `Canada`, etc.
By default, apolitical names `alpha`, `bravo`, ... `zulu` are used as the regions.

Logically, the location of an IP address is not powerful evidence that an APT is located in the same region. E.g. use of an IP in `delta` does not mean that the APT is based there.
However, patterns may emerge that do increase the confidence of geolocating an APT

  * e.g. if an APT's C2 servers are always hosted in the `delta` region, and all victims are from the `alpha` or `echo` region, and `alpha` and `echo` are known adversaries of `delta`, then perhaps the APT is `delta`-based APT targeting `alpha` and `echo`

Currently, regions in `autovnet` are cosmetic classifiers with no intrinsic meaning, but their existence increases roleplay potential.

Regions are distributed as part of the game configuration.


## APT Families

An APT Family is a pool of infrastructure from which individual APT profiles can be created.

The real-world analog is a logical Advanced Persisent Threat actor / sponsor, similar to the "Suspected attribution" from [Mandiant](https://www.mandiant.com/resources/apt-groups).

An APT Family's infrastructure likely spans many regions.

Many APT profiles can belong to the same Family.

APT Families add roleplaying potential, and the potential for clustering individual APTs based on some (intentional) commonality shared within a Family.

APT Families are distributed as part of the game configuration.

## APT Profiles

An APT profile is a concrete set of infrastructure that is logically available to an APT to use for its malicous activity.

In `autovnet`, individual players generate APT profiles, and they are not shared between players by default.

  * By default, "sharing between players" happens at the APT Family level
  * Players can choose to share their APT profiles by placing them in a [shared directory](https://autovtools.gitlab.io/autovrtfm/autovnet-docs/usage/collaboration/fs/) or otherwise distributing them
    - Be mindful that the likelihood of commands failing due to IP conflicts increases as APT profile size decreases


When generating an APT profile, the player provides a `size`, when controls how much infrastructure will be allocated.

  * Because of how `autovnet` works, "allocated" doesn't mean much - the "infrastructure" is just a random IP stored in a text file
    - Future `autovnet` commands might read that text file and use that random IP
    - So the IP is "allocated", with 0 actual resource cost
    - The real allocation happens when you actually try to use the IP, and because of math, it's extremely unlikely to conflict with any other players.

  * The `sizes` and their meanings are defined in `templates.yaml`, distributed as part of the game config

To avoid "attribution by proximity" (i.e. attributing an entire block of IP addresses to a particular actor due to knowledge of how `autovnet` does IP partioning), APT profiles use much smaller CIDR blocks (often, `/32`).

Because there are potentionally _billions_ of IPs in the game, we have to be careful: going too overboard with infrastructure could result in APT profile files that are GBs large.
We want to keep them manageable, to save our disk space and let them fit in RAM.

Because of this, super big APT profiles have some features disabled.

  * `dns`: reasonably-sized APT profiles pre-register DNS names for each IP in the profile
  * `geo`: reasonably-sized APT profiles pre-geolocate each IP

## Commands

### Create a New APT Profile

Generate a new APT profile.

**This does not activate the APT profile**

```bash
$ autovnet apt new
[+] APT Family: HORSE
[+] Assigning infrastructure (size=default)...
_  _ _  _ ____ ____ _ ____ ___     _  _ ____ ____ ____ ____ 
|__| |  | |__/ |__/ | |___ |  \    |__| |  | |__/ [__  |___ 
|  | |__| |  \ |  \ | |___ |__/    |  | |__| |  \ ___] |___ 
                                                            

[+] name: hurried_horse
[+] tun: 32
[+] proxy: 160
[+] listen: 32
[+] total: 224
[*] Creating 224 DNS records...
[+] dns: 224 records created
[+] profile: /opt/rtfm/apt/profiles/hurried_horse/hurried_horse.yaml
[+] activate: autovnet apt shell hurried_horse
```

### Activate an APT Profile

Drop into a shell with the APT profile active.

**The APT profile is only active in the current shell**

To help you track which profile, if any, is active, `autovnet` will attempt to update your shell prompt.

There is also a `show` command that will display your active APT's stats.

```bash
(rtfm) $ autovnet apt shell hurried_horse 
(hurried_horse) $ autovnet apt show
_  _ _  _ ____ ____ _ ____ ___     _  _ ____ ____ ____ ____ 
|__| |  | |__/ |__/ | |___ |  \    |__| |  | |__/ [__  |___ 
|  | |__| |  \ |  \ | |___ |__/    |  | |__| |  \ ___] |___ 
                                                            

[+] name: hurried_horse
[+] tun: 32
[+] proxy: 160
[+] listen: 32
[+] total: 224
[+] profile: /opt/rtfm/apt/profiles/hurried_horse/hurried_horse.yaml
```

Now that a profile is active, we can verify that new `autovnet` commands (in the same shell) now draw IPs from the APT's pool, not the global `autovnet` pool.

In this case, there are `224` IPs available to the APT.
  * The IPs are categorized (`tun`, `proxy`, `listen`, etc.)
  * The `tun` command uses the `tun` pool, the `proxy` command uses the `proxy` pool, etc.

The easiest way to prove that your "random" IP addresses are being drawn from your APT pool is with some experimentation.

1. Generate a very large number of "random" IPs
2. Count the number of unique IPs
3. If we generated enough "random" IPs, we expect exactly `224` unique IPs, since those are the only "valid" IPs to draw from

```bash
(hurried_horse) autovnet net ip -c 8192 | sort -u | wc -l
224
```
When we run the same test with no active APT profile, IPs are drawn from the whole `autovnet` pool, which is so large, we expect few (if any) duplicates.

```bash
(hurried_horse) $ exit

(rtfm) $ autovnet apt show
[!] No active profile (AVN_APT) not set

(rtfm) $ autovnet net ip -c 8192 | sort -u | wc -l
8192
```

### DNS Records

Depending on the `size` of your APT, you may have DNS names pre-registered for your infrastructure.

All but the largest sizes of APTs will pre-register DNS names by default.

Extremely large APT profiles should not pre-register DNS names, since it will make the profile very large, and it will create a lot of DNS records that will probably never be used.
If you need a large APT profile, just manually create your DNS records with the [`autovnet rtfm dns` command(s)](../rdn/dns.md).

Whether a new APT will pre-create these records is based on the `size`, and is controlled by `templates.yaml` in the game config.

The hostname / IP mapping is stored locally in the APT profile.

#### Lookup

You can look up a host nmae by IP address locally (no actual DNS query, just checking your profile) with the `autovnet apt dns` command.
**You must have an APT profile activated**

```bash
(hurried_horse) $ IP=$(autovnet net ip); echo -e "IP: ${IP}\nHOSTNAME: $(autovnet apt dns ${IP})"
IP: 244.156.107.60
HOSTNAME: clear.response.avn
```

Tab completion is also helpful here, and allows you to potentionally "resolve" your IPs of interest without actually running the command.

```bash
(hurried_horse) $ autovnet apt dns 240[TAB]
240.145.196.80   -- orange.nature.avn
240.145.196.81   -- joyous.possibility.avn
240.145.196.82   -- parsimonious.occasion.avn
240.145.196.83   -- abnormal.mistake.avn
240.159.111.205  -- unkempt.mess.avn
...
```

You can also just search your APT profile file, if desired.

#### Unregister / Register

If desired, you can manually register / unregister the batch of DNS mappings stored in your APT profile.

We can demonstrate this with a oneliner that

  * Draws a random IP
  * Looks up the associated DNS name (from profile)
  * Performs an actual DNS query on the DNS name

```bash
(hurried_horse) $ IP=$(autovnet net ip); HOSTNAME=$(autovnet apt dns ${IP}); RESOLVED=$(dig ${HOSTNAME} +short); echo -e "IP: ${IP}\nHOSTNAME: ${HOSTNAME}\nRESOLVED: ${RESOLVED}" 
IP: 254.32.142.251
HOSTNAME: minor.penalty.avn
RESOLVED: 254.32.142.251
```

After unregistering, the DNS resolution fails, leaving the `RESOLVED` field blank in our test

```bash
(hurried_horse) $ autovnet apt unregister
[*] Unregister 224 DNS records...
[+] dns: 224 records deleted

(hurried_horse) $ IP=$(autovnet net ip); HOSTNAME=$(autovnet apt dns ${IP}); RESOLVED=$(dig ${HOSTNAME} +short); echo -e "IP: ${IP}\nHOSTNAME: ${HOSTNAME}\nRESOLVED: ${RESOLVED}"
IP: 252.230.175.124
HOSTNAME: childlike.relative.avn
RESOLVED:
```

After re-registering, the `RESOLVED` field returns, since the DNS query succeeds.

```bash
(hurried_horse) $ autovnet apt register
[*] Creating 224 DNS records...
[+] dns: 224 records created

(hurried_horse) $ autovnet.local git:(dev) ✗ IP=$(autovnet net ip); HOSTNAME=$(autovnet apt dns ${IP}); RESOLVED=$(dig ${HOSTNAME} +short); echo -e "IP: ${IP}\nHOSTNAME: ${HOSTNAME}\nRESOLVED: ${RESOLVED}"
IP: 255.242.225.112
HOSTNAME: thoughtless.blind.avn
RESOLVED: 255.242.225.112
```

### Geolocate

`geo` is exactly like `dns`, but it returns the `region` of the IP, not the DNS name.

```bash
(hurried_horse) $ autovnet apt geo $(autovnet net ip)
delta
```
