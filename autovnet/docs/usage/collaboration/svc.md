# Shared Services

[TOC]

## Intro

Let's assume your favorite service (C2 server, file server, web app, etc.) is running on your local machine just fine.

How can your share / expose this service so other game participants on a "public" IP that others can reach?

# Public Services

Shared services that are accessible to anyone on the game network are trivially easy - that's what `autovnet rtfm listen` does.

## Example

```bash
# Share a local python server with everyone
$ python3 -m http.server -b 127.0.0.1 8000

$ autovnet rtfm listen 80 -p 8000
[+] autovnet rtfm listen greasy_dad
[+] greasy_dad | 10.221.119.32:80 => 127.0.0.1:8000
```

# Red Only / `autovnet` player only services

The interesting kind of shared services are the secure kind - only players with the `autovnet` game config are allowed to access them.

When you import the `autovnet` config from the server, you are given some crypto keys that your `autovnet` client will use for future authentication to the server,
and (as in [Shared Directories](#shared-directories)) peer-to-peer between players

One of these pre-shared secrets is a TLS certificate and key. The `autovnet rtfm svc` command allows secure "peer-to-peer" service sharing between players.

The traffic on the wire is encrypted via TLS, and the export-er and the mount-er of the service are mutually authenticated via mTLS.
(Both parties must present the pinned cert / key before the real service is reachable).

This is implemented using [stunnel](https://www.stunnel.org/static/stunnel.html).

Imagine Player 1 wants to share a private web server with Player 2, but doesn't want Blue Team to be able to access it.

Conceptually:

* Player 1 will bind the webserver to `127.0.0.1:8000`, so only Player 1 can access it.
* Player 2 will also bind to `127.0.0.1:8000`, which only Player 2 can access.
    - `stunnel` will create a magic tunnel between Player 1's `127.0.0.1:8000` and Player 2's `127.0.0.1:8000`
    - Packets sent to Player 2 `127.0.0.1:8000` will be encrypted via TLS and sent over the tunnel to Player 2's `127.0.0.1:8000`
* No rogue Blue Teamer can access the private web server, because the only way to get to it is via the tunnel, and you can only use the tunnel if you have the pre-shared `rtfm.key`

**Remember to leave the default IPs alone unless you know what you are doing - you want `127.X.X.X`, or it isn't secure any more** :)

## Example

Here is a quick example showing how easy it is to use in practice.

Player 1 starts a **private** (localhost only) web server
```bash
$ python -m http.server -b 127.0.0.1 8000
Serving HTTP on 127.0.0.1 port 8000 (http://127.0.0.1:8000/) ...
```

Player 1 exports their private service as a player-only service
```bash
autovnet rtfm svc export -p 8000
[+] autovnet rtfm svc export sincere_preference
[+] exporting: 127.0.0.1:8000
[+] mount: autovnet rtfm svc mount -C sincere_preference 10.211.108.32:65006
```
Note that the "public" port (here, `65006`) is random - you only want other players to try to interact with your service,
so we don't care how legitimate it would look to Blue Team at all.


Then, share the `mount` command with your fellow players.

Player 2 can then mount the service (and append their own `-p PORT` according to their preference):
```bash
autovnet rtfm svc mount -C sincere_preference 10.211.108.32:65006 -p 8000
[+] autovnet rtfm svc mount sincere_preference
[+] mounted locally: 127.0.0.1:8000
```

Now, Player 2 can access `127.0.0.1:8000` as if it were Player 1's `127.0.0.1:8000`.

The packets get to where they need to go, and the only "public" hop `10.211.108.32:65006` will drop reject any connections that do not present the `rtfm.key` TLS key.

```bash
$ curl 127.0.0.1:8000
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Directory listing for /</title>
</head>
<body>
<h1>Directory listing for /</h1>
<hr>
<ul>
</ul>
<hr>
</body>
</html>
```

You can use this approach to secure any Red-Only TCP collaboration services, and more features that build on this primitive are coming soon!

