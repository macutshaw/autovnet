# Collaboration

[TOC]

In a multiplayer wargame environment, high-bandwidth communication and easy collaboration is key to success.

## Collaboration - Simple View

This is a peek at how collaboration between players works:

![rtfm_p2p_simple.svg](../../img/rtfm_p2p_simple.svg)

In this example, the `export`'er is running a Red Team only applciation on `127.0.0.1:1337`.

But after `mount`'ing the service, the second player now is _also_ listening on `127.0.0.1.1337`

When Player 2 (the `mount`'er) connects to their own `127.0.0.1:1337`, the request somehow magically makes it to Player 1 (the `export`'er's) `127.0.0.1:1337`.

> But that's localhost!

Well yes, but it's also _magic_ ;)

## Collaboration - Detailed View

This diagram should help demystify a little more:

![rtfm_p2p_detailed.svg](../../img/rtfm_p2p_detailed.svg)

Basically, the `autovnet server` **super securely** mediates the connection between both peers.

* For more details on this mediation, see [how `autovnet` listeners work](../../rtfm/autovnet.md#reverse-callback).

Both players can send requests to `127.0.0.1:1337`, but the requests are always actually handled by the player that `export`'ed the service.

Many, many players can `mount` the same service at a time, so be a team player and collaborate!

## How _secure_ is _secure_? Can Blue Team see my Red Team Only services?

Way more secure than you could possibly actually need - the foundation is [stunnel](https://www.stunnel.org/), a real program.

Don't worry - Blue Team are cryptographically incapable of connecting to your protected collaboration services.
By default, only other players that share your `autovnet` game configuration can collaborate with you.

`autovnet` does crypto good, and transparently, cryptographically validates that every player who wants to collaborate is a trusted player.

* The quick version is that all players are given a secret game config that contains game keys, and those game keys allow players to prove to each other that they are part of the same team
