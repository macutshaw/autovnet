#!/bin/bash
set -e

if [ -z "${SSH_ORIGINAL_COMMAND}" ]; then
    sleep infinity
fi
set -eu
# https://security.stackexchange.com/questions/118688/security-of-only-allowing-a-few-vetted-commands-using-ssh-original-command
# WARNING: Probably vulnerable
SOCAT="/usr/bin/socat"
ALLOWED="socat"
set -- ${SSH_ORIGINAL_COMMAND}
if [ "$1" = "${ALLOWED}" ]; then
    # Hopefully fine
    shift
    exec "${SOCAT}" "$@"
fi
echo "Only ${ALLOWED} is allowed"
