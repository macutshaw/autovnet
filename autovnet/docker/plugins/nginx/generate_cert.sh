#!/bin/bash

CERTS="/tmp/certs"
KEY="${CERTS}/key.pem"
CERT="${CERTS}/cert.pem"

mkdir -p "${CERTS}"
openssl req -x509 -newkey rsa:4096 -keyout "${KEY}" -out "${CERT}" -sha256 -days 365 -nodes -subj '/CN=localhost'
