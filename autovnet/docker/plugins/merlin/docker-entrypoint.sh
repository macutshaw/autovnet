#!/bin/bash

set -euo pipefail

BIN_SRC="/opt/merlin-bin"
BIN_DST="/opt/merlin/bin"
# Copy binaries to mounted volume
if [ ! -d "${BIN_DST}" ]; then
    mv "${BIN_SRC}" "${BIN_DST}"
fi

merlin
