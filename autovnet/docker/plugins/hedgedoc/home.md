# Home

Add links to any new pages here, at [/home](/home)

**If your page cannot be found by traversing a chain of links from /home, assume that neither your friends nor future-you will be able to find it :)**

## How-To: Subpages

Just create a [markdown link](https://www.markdownguide.org/basic-syntax/#links) with a desination like `/link-example`, [like this](/link-example).

When you click it, it makes a new page named `link-example`! Neat.

Click any of these links to create a new page:
* [foo](/foo)
* [bar](/bar)
* [foobar](/foobar)


## How-To: Formatting

If you want to paste a block of code

```bash
echo "Do it like this!"
echo "Code goes between the triple backticks"
echo "The language goes after the opening backticks"
```

```python
print("Wow, so pretty!")
```

You can also format a single line of code like `this`, by using single backticks.

See https://www.markdownguide.org/tools/hedgedoc/ for all supported formatting.

