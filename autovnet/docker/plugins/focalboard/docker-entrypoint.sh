#!/bin/bash
set -eu

FOCALBOARD_CONFIG_TEMPLATE="/etc/focalboard/config.json.template"
FOCALBOARD_CONFIG="/etc/focalboard/config.json"
POSTGRES_PORT=${POSTGRES_PORT:-5432}

envsubst < "${FOCALBOARD_CONFIG_TEMPLATE}" > "${FOCALBOARD_CONFIG}"

while ! 2>/dev/null echo -n > "/dev/tcp/${POSTGRES_HOST}/${POSTGRES_PORT}"; do
    echo "Waiting for ${POSTGRES_HOST}..."
    sleep 1
done

exec /opt/focalboard/bin/focalboard-server --config "${FOCALBOARD_CONFIG}"
