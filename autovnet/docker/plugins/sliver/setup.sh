#!/bin/bash
set -euo pipefail

BIN_PATH="/opt/sliver-bin"
SLIVER_USER="${SLIVER_USER:-default}"
CLIENT_CONFIGS="${HOME}/.sliver-client/configs"

export PATH="${BIN_PATH}:${PATH}"
sliver-server unpack --force
# The go dir is massive, keep it in the image
mv "${HOME}/.sliver/go" "${HOME}/.sliver-go"

# Generate local configs
echo "Generating operator configs ..."
mkdir -p "${CLIENT_CONFIGS}"
#sliver-server operator --name "${SLIVER_USER}" --lhost "${MULTIPLAYER_HOST:-127.0.0.1}" --lport "${MULTIPLAYER_PORT:-31337}" --save "${CLIENT_CONFIGS}/${SLIVER_USER}.json"
