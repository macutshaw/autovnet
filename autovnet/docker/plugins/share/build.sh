#!/bin/bash
set -eu
if [ -z "$1" ]; then
    echo "Usage: ./build.sh OUT_DIR"
fi
DOCKER_BUILDKIT=1 docker build . --target artifacts --output "$1"
