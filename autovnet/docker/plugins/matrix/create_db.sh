#!/bin/sh

echo -e "${POSTGRES_PASSWORD}\n${POSTGRES_PASSWORD}" | createuser -U postgres -s dendrite -P
for db in userapi_accounts userapi_devices mediaapi syncapi roomserver signingkeyserver keyserver federationsender appservice naffka; do
    createdb -U dendrite -O dendrite dendrite_$db
done
