#!/bin/bash

set -euo pipefail
LIGOLO_NG_PROXY_ARGS="${LIGOLO_NG_PROXY_ARGS:--selfcert}"

BIN_SRC="/opt/ligolo-ng-bin"
BIN_DST="/opt/ligolo-ng/bin"
# Copy binaries to mounted volume
if [ ! -d "${BIN_DST}" ]; then
    mv "${BIN_SRC}" "${BIN_DST}"
fi

exec proxy ${LIGOLO_NG_PROXY_ARGS}

