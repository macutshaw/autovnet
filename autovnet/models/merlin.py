from enum import Enum


class ListenProto(str, Enum):
    http = "http"
    https = "https"
    http2 = "http2"
    http3 = "http3"
    h2c = "h2c"
