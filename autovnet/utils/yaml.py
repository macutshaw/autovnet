import subprocess

import yaml

# https://stackoverflow.com/questions/37200150/can-i-dump-blank-instead-of-null-in-yaml-pyyaml/37201633#37201633
def represent_none(self, _):
    return self.represent_scalar("tag:yaml.org,2002:null", "")


yaml.add_representer(type(None), represent_none)


def yaml_dump(d: dict) -> str:
    return yaml.safe_dump(d, default_flow_style=False, sort_keys=False)


def shell_exec(cmd: str) -> str:
    """Execute a shell command and return its output

    DANGER: available within a jinja template, to allow arbitrary dynamic values to
    be rendered in a template.

    DO NOT RENDER TEMPLATES FROM UNTRUSTED SOURCES.
    """
    output = subprocess.check_output(cmd, shell=True).decode()
    if output.endswith("\n"):
        # This might be surprising, but most output will be newline terminated,
        # and we don't want to spam '|head -n1' in every shell_exec call
        output = output[:-1]
    return output
