from contextlib import contextmanager
from pathlib import Path

# Kali moved rt_tables without creating a symlink, neat
KNOWN_RT_TABLES_PATHS = [
    Path("/etc/iproute2/rt_tables"),
    Path("/usr/lib/iproute2/rt_tables"),
]


def find_rt_tables_path():
    rt_tables_path = None
    for test_path in KNOWN_RT_TABLES_PATHS:
        if test_path.is_file():
            rt_tables_path = test_path
            break

    if rt_tables_path is None:
        raise Exception(
            f"Failed to find path to rt_tables! Checked: {KNOWN_RT_TABLES_PATHS}"
        )

    return rt_tables_path


@contextmanager
def yield_none(items=1):
    yield (None,) * items
