import os
import gzip
import shutil
import shlex
import subprocess
from pathlib import Path
from typing import Optional, List

import typer

from ...autovnet import AutoVnet, avn
from autovnet.models import Mount
from autovnet.utils import (
    compose_up_ctx,
    complete_codename,
    complete_files,
    gen_codename,
    read_env_file,
    rand_port,
    wait_url_open,
    open_url,
    ensure_cert,
    pg_dumper,
)

app = typer.Typer()

MOUNT_TYPE = "nginx"


@app.command()
def serve(
    dest_ip: Optional[str] = typer.Option(
        "127.0.0.1",
        "-d",
        "--dest-ip",
        help="[ADVANCED] Bind address for the server (anything but 127.X.X.X may allow anyone to access!).",
    ),
    dest_http_port: Optional[int] = typer.Option(
        80, "-p", "--dest-http-port", help="Local bind port for the server (HTTP). (default: 80)."
    ),
    dest_https_port: Optional[int] = typer.Option(
        443, "-P", "--dest-https-port", help="Local bind port for the server (HTTPS). (default: 443)."
    ),
    no_browser: Optional[bool] = typer.Option(
        False, "-N", "--no-browser", help="If set, do not open the default browser."
    ),
    no_autoindex: Optional[bool] = typer.Option(
        False, "-I", "--no-autoindex", help="If set, disable nginx autoindex (directory listing)."
    ),
    conf_template: Optional[str] = typer.Option(
        None, "-C", "--conf-template", help="[ADVANCED] Alterate nginx config template."
    ),
    generate_cert: Optional[str] = typer.Option(
        None, "-G", "--generate-cert", help="[ADVANCED] Alterate script for generating an SSL cert."
    ),
    serve_dir: Optional[str] = typer.Argument(
        None,
        autocompletion=complete_files,
        help="Previous codename to load",
    ),
):
    """Serve a directory with nginx"""

    # Prepare compose
    docker_path = avn.docker_path(f"plugins/{MOUNT_TYPE}/")

    if conf_template is None:
        conf_template = docker_path / "default.conf.template"

    if generate_cert is None:
        generate_cert = docker_path / "generate_cert.sh"

    autoindex = "on"
    if no_autoindex:
        autoindex = "off"

    proto = "http"
    nginx_http_bind = f"{dest_ip}:{dest_http_port}"
    nginx_https_bind = f"{dest_ip}:{dest_https_port}"
    url = f"{proto}://{nginx_http_bind}"
    env = {
        "NGINX_HTTP_BIND": nginx_http_bind,
        "NGINX_HTTPS_BIND": nginx_https_bind,
        "NGINX_DEFAULT_TEMPLATE": conf_template,
        "NGINX_GENERATE_CERT": generate_cert,
        "NGINX_DIR": serve_dir,
        "NGINX_AUTOINDEX": autoindex,
    }

    codename = gen_codename()

    project = f"{MOUNT_TYPE}_{codename}"

    typer.echo(f"[+] serving : {serve_dir}")
    typer.echo(f"[+] connect : xdg-open {url}")

    with compose_up_ctx(docker_path, env=env, project=project, quiet=True) as (
        compose_thread,
        compose_stop,
    ):
        typer.echo(f"[*] Waiting for {MOUNT_TYPE} ...")
        ok = wait_url_open(url, thread=compose_thread)
        if ok:
            typer.echo(f"[+] {MOUNT_TYPE} is ready!")
        elif ok is False:
            typer.echo(f"[!] {MOUNT_TYPE} may still be initializing")

        if not no_browser:
            open_url(url)

        compose_thread.join()
