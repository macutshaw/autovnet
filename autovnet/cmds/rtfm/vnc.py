import re
import os
import time
import shlex
import subprocess
from enum import Enum
from pathlib import Path
from typing import Optional, List

import typer

from ...autovnet import AutoVnet, avn
from autovnet.models import Mount, StreamQuality
from autovnet.utils import (
    compose_up,
    compose_down,
    complete_codename,
    gen_password,
    read_env_file,
    write_env_file,
    rand_port,
)

app = typer.Typer()


@app.command()
def export(
    networks: Optional[List[str]] = typer.Option(
        None, "-n", "--network", help="Simulated network to draw a random IP from."
    ),
    dest_ip: Optional[str] = typer.Option(
        "127.0.0.1",
        "-d",
        "--dest-ip",
        help="[ADVANCED] Bind address for the server (anything but 127.X.X.X may allow anyone to access!).",
    ),
    dest_port: Optional[int] = typer.Option(
        None, "-p", "--dest-port", help="Local bind port for the server. (default: random)"
    ),
    display: Optional[str] = typer.Option(
        os.environ.get("DISPLAY", ":0"), "--display", help="[ADVANCED] The X display to share."
    ),
    x_socks: Optional[str] = typer.Option(
        "/tmp/.X11-unix/", "-x", "--x-socks", help="[ADVANCED] The directory containing X sockets."
    ),
    x_authority: Optional[str] = typer.Option(
        os.environ.get("XAUTHORITY", str(Path("~/.Xauthority").expanduser())),
        "-X",
        "--x-auth",
        help="[ADVANCED] The path to the .Xauthority file",
    ),
    require_password: Optional[bool] = typer.Option(
        False,
        "-P",
        "--require-password",
        help="Require password to connect. Not really more secure, but might make you feel better.",
    ),
    vnc_scale: Optional[str] = typer.Option(
        "",
        "-s",
        "--scale",
        help="Scale your screenshare resolution. Can be a resolution, fraction, decimal, etc. See '-scale': https://linux.die.net/man/1/x11vnc.",
    ),
    vnc_env: Optional[str] = typer.Option(
        None, "-e", "--env-file", help="[ADVANCED] Environment file to pass to docker-compose."
    ),
    payload_dir: Optional[str] = typer.Option(
        avn.data_path("vnc"), "-D", "--payload-dir", help="Directory to save command metadata."
    ),
    codename: Optional[str] = typer.Argument(
        None,
        autocompletion=complete_codename,
        help="Previous codename to load",
    ),
):
    """Run a view-only VNC server to screenshare with other players.

    The server is locked down to disallow any remote control.
    Because `autovnet rtfm svc` is used, only other autovnet players can mount and connect to your screen.

    Currently uses x11vnc under the hood, so it probably won't work if you're using Wayland or anything not standard Xorg.

    The default behavior is to share the current / primary screen at native resolution.
    Since most VNC clients do not support client-side scaling, players with larger-than-average monitors should generally
    --scale their output down to something more reasonable for others to consume.

    --scale 1600x900 is generally a good choice, which is legible on 4k monitors while behaving reasonably on a 1920x1080 (or less) display / laptop.

    If necessary, you can simply export screenshares at the same time, with different --scale options.
    Then, players can choose which stream resolution is most comfortable for them.

    If players are remote, your stream quality is limited by the host's upload speed, but at least on a local virtual, 10Gbps network, the quality can be pristine.
    Clients have control of the stream quality, so instruct your players to mount the stream at an acceptable --quality as appropriate for your network.

    The default settings for clients sacrifices some stream quality for massively reduced data transfer.
    """

    if dest_port is None:
        dest_port = rand_port()

    ctx = None

    if codename is None:
        # New svc
        ctx = lambda: avn.svc(
            networks=networks, dest_ip=dest_ip, dest_port=dest_port, payload_dir=payload_dir, echo=False
        )
    else:
        # Loaded svc
        ctx = lambda: avn.svc_from_codename(codename, payload_dir=payload_dir, echo=False)
        # Peek at listener config to grab the values we need
        ln = avn.listener_from_codename(codename, payload_dir=payload_dir)
        model = ln.lns[0]
        dest_ip = model.next_dest_ip
        dest_port = model.next_dest_port

    docker_path = avn.docker_path("plugins/vnc/")

    vnc_password = ""
    if require_password:
        vnc_password = gen_password(secure=False)

    # WARNING: goofy behavior
    # We want to allow cli --scale and password options to override previously saved values,
    # but we don't want to override all the advanced stuff the user probably got right the first time

    tmp_env = {
        "DISPLAY": f"{display}",
        "XAUTHORITY": f"{x_authority}",
        "VNC_BIND": f"{dest_ip}:{dest_port}",
        "VNC_XSOCKS": f"{x_socks}",
        "VNC_PASSWORD": f"{vnc_password}",
        "VNC_SCALE": f"{vnc_scale}",
    }

    env = {}
    save_env = None
    if vnc_env is not None:
        # Power user override
        env = read_env_file(vnc_env, parent=False)
    elif codename is not None:
        # Carefully load the previously dumped ENV so we start with the correct options
        env_path = Path(payload_dir) / codename / "vnc.env"
        env = read_env_file(env_path, parent=False)

    # Start with CLI options, then carefully apply any loaded overrides
    for key in env.keys():
        if key not in tmp_env:
            typer.echo(f"[!] Ignoring unknown environment variable: {key}")
        else:
            tmp_env[key] = env[key]
    env = tmp_env
    # Re-apply any scale / password overrides
    if not env["VNC_PASSWORD"] and vnc_password:
        # User asked for a temporary password
        env["VNC_PASSWORD"] = vnc_password
    if vnc_scale:
        # User specified --scale, ignore any loaded value
        env["VNC_SCALE"] = vnc_scale

    if codename is None:
        # Save these options for next time
        save_env = env.copy()

    # Set variables that user does not control
    env["UID"] = str(os.geteuid())
    env["GID"] = str(os.getegid())

    with ctx() as (proc, codename, remote_addr):
        if save_env is not None:
            # We need to save the options to disk now that we know the codename
            env_path = Path(payload_dir) / codename / "vnc.env"
            env_path.parent.mkdir(parents=True, exist_ok=True)
            write_env_file(save_env, env_path)
        project = f"vnc_{codename}"
        mount_cmd = f"autovnet rtfm vnc mount -C {codename} {remote_addr}"

        typer.echo(f"[+] autovnet rtfm vnc export {codename}")
        typer.echo(f"[+] mount: {mount_cmd}")
        if require_password:
            typer.echo(f"[+] password: {vnc_password}")
        compose_up(docker_path, env=env, project=project, quiet=True)


@app.command()
def mount(
    dest_ip: Optional[str] = typer.Option(
        "127.0.0.1",
        "-d",
        "--dest-ip",
        help="[ADVANCED] Destination IP you will use to access the service. Anything but 127.X.X.X may be insecure!",
    ),
    dest_port: Optional[int] = typer.Option(
        None,
        "-p",
        "--dest-port",
        help="[ADVANCED] Destination port you will use to access the service (default: random).",
    ),
    payload_dir: Optional[str] = typer.Option(
        avn.data_path("vnc"), "-D", "--payload-dir", help="Directory to save command metadata."
    ),
    viewer: Optional[str] = typer.Option(
        "xtightvncviewer", "-V", "--viewer", help="[ADVANCED] The vnc client program to run."
    ),
    no_vnc_options: Optional[bool] = typer.Option(
        False,
        "-N",
        "--no-vnc-options",
        help="[ADVANCED] Only pass IP:PORT to --viewer, no extra options. Disables --quality settings.",
    ),
    quality: Optional[StreamQuality] = typer.Option(
        None,
        "-Q",
        "--quality",
        help="The stream quality to request. If bandwidth is limited, work with the presenter to choose an approriate setting.",
    ),
    codename: Optional[str] = typer.Option(None, "-C", "--set-codename", help="Set the codename for the mount."),
    opt: str = typer.Argument(
        ...,
        autocompletion=complete_codename,
        help="IP:PORT or codename to mount.",
    ),
):
    """Mount and connect to another player's view-only vnc export.

    Don't worry, you can't accidentally mess up the presenter - your session is forced to be view-only by the server,
    so you cannot move their mouse, type, or resize their remote screen.


    The default settings for clients sacrifices some stream quality for massively reduced data transfer.
    The --quality level you choose impacts the amount of data the presenter has to "upload" to your system.

    If they are remote (not on the same virtual infrastructure as you), then all players watching the stream will be
    limited by the upload speed of the presenter.

    It's likely that many players could concurrently watch a single presenter, as if in a classroom setting,
    but this implementation is absolutely not optimal for that use case, since the amount of data the presenter must upload
    scales linearly with the number of connected clients - each client receives a separate, complete stream from the presenter.

    Classroom-style presentations will work best if the presenter has excellent upload speed from their ISP,
    or better, if the machine running the vnc export is centrally located in your game infrastructure,
    likely hosted on-prem with corpoarate internet or in the cloud.

    But the primary use case is ad-hoc sharing where 1 presenter recieves technical assistance from a few mentors,
    or a single preseneter is watched by a small team (e.g. < ~10 players) at a time, with other players starting
    their own exports as needed for real-time collaboration.

    A more scalable solution for classroom-style streaming, with 1 presenter and N watchers may eventually be added.
    As a workaround, a centrally hosted VM (or fellow player with better upload speed) could mount the presenter's stream, then `vnc export` their
    own display for others to mount.
    Since ISP upload speed is by far the bottleneck, this strategy should signficantly increase the number of players that can watch concurrently.

    Quality Levels

    * `potato`: The lowest quality possible. If even `potato` is too much for your upload speed, RIP.

        - 4k Matrix Rain torture test: ~3 MiB/s up
        - 4k active terminal: ~200 KiB/s up

    * `low`: Try to maintain legibility, but sacrifice as much quality as possible to minimize bandwidth needs.

        - 4k Matrix Rain torture test: ~4 MiB/s up

    * `default`: The default quality, aims to be reasonably crisp while still signficantly reducing bandwidth needs.

        - 4k Matrix Rain torture test: ~5 MiB/s up
        - 4k active terminal: ~250 KiB/s up

    * `mid`: Middle ground between `default` and `high`. Approximately the same quality as xtightvncviewer's default.

        - 4k Matrix Rain torture test: ~6 MiB/s up

    * `high`: Almost full quality, very little lossy compression.

        - 4k Matrix Rain torture test: ~15 MiB/s up
        - 4k active terminal: ~1 MiB/s up

    * `ultra`: Minimum compression, low CPU load and close to lossless.

        - 4k Matrix Rain torture test: ~30 MiB/s up

    * `raw`: All the bytes, mimimum CPU load, but extremely bandwidth intensive. Only use if you and the presenter are local.
        - 4k Matrix Rain torture test: ~200 MiB/s up
        - 4k active terminal: ~10 MiB/s up

    With the rough bandwidth estimates above, it should be possible for a remote player with a paltry 5 Mbps ISP upload speed
    (typical for residential internet in many regions) to share default quality, mostly terminal streams with 1-2 concurrent watchers.

    """

    # Save the user-specified value
    user_quality = quality
    if quality is None:
        quality = StreamQuality.default

    if dest_port is None:
        dest_port = rand_port()

    cmd = ["autovnet", "rtfm", "svc", "mount", "--mount-type", "vnc"]

    codename_pattern = re.compile("^[a-z_]+$")
    if codename is not None:
        # Check for existing dir
        old_config = Path(payload_dir) / codename / "mount.yaml"
        if old_config.is_file():
            typer.echo(f"[!] {old_config} already exists! Loading previous config..")
            opt = codename
            codename = None

    if codename_pattern.match(opt):
        codename = opt
        # Loading a codename, peek at the mount config to get the local addr
        mnt = Mount.load(payload_dir, codename)
        dest_ip = mnt.svc_host
        dest_port = mnt.svc_port
        # Allow user to override the stored quality
        if user_quality is None:
            quality = mnt.vnc_quality
    else:
        # Pass thru any svc options
        if dest_ip != "127.0.0.1":
            cmd.extend(["-d", dest_ip])
        if dest_port is not None:
            cmd.extend(["-p", str(dest_port)])

        if codename is not None:
            cmd.extend(["-C", codename])

    cmd.extend(["-D", str(payload_dir)])
    cmd.append(opt)

    vnc_cmd = [viewer]
    if not no_vnc_options:
        # -viewonly is enforced by the server, this is just cosmetic to prevent drawing a local cursor
        vnc_cmd.append("-viewonly")

        # Set encoding preferences for major bandwidth savings
        # (looks like 'tight' is always chosen)
        encodings = "copyrect tight zlib hextile"
        if quality == StreamQuality.raw:
            encodings = "raw"
        vnc_cmd.extend(["-encodings", encodings])

        compress_level = None
        quality_level = None
        if quality == StreamQuality.potato:
            compress_level = 9
            quality_level = 0
        elif quality == StreamQuality.low:
            compress_level = 9
            quality_level = 3
        elif quality == StreamQuality.default:
            compress_level = 8
            quality_level = 5
        elif quality == StreamQuality.mid:
            compress_level = 7
            quality_level = 6
        elif quality == StreamQuality.high:
            compress_level = 7
            quality_level = 9
        elif quality == StreamQuality.ultra:
            compress_level = 1
            quality_level = 9
        elif quality == StreamQuality.raw:
            # N/A for raw
            pass
        else:
            raise ValueError(f"[-] Unknown quality: {quality}")

        if compress_level is not None:
            vnc_cmd.extend(["-compresslevel", str(compress_level)])
        if quality_level is not None:
            vnc_cmd.extend(["-quality", str(quality_level)])

    vnc_cmd.append(f"{dest_ip}:{dest_port}")

    vnc_proc = None
    proc = None
    attempts = 300

    if codename is not None:
        typer.echo(f"[+] autovnet rtfm vnc mount {codename}")
    try:
        proc = subprocess.Popen(cmd, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
        try:
            ret = proc.wait(1)
            raise Exception(f"[-] svc mount exited early! Code: {ret} ({shlex.join(cmd)})")
        except subprocess.TimeoutExpired:
            pass
        while attempts > 0 and proc.poll() is None and (vnc_proc is None or vnc_proc.poll() is not None):
            vnc_proc = subprocess.Popen(vnc_cmd, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
            try:
                ret = vnc_proc.wait(1)
                # vncviewer probably failed to connect, retry
                if ret != 0:
                    typer.echo(f"[*] Connection failed ({attempts} attempts left...)")
                time.sleep(1)
                attempts -= 1
            except subprocess.TimeoutExpired:
                # Looks like vncviewer connected
                break
    finally:
        if codename is not None and user_quality is not None:
            # WARNING: goofy behavior
            # If you didn't set -C, then your quality preference won't be saved
            try:
                mnt = Mount.load(payload_dir, codename)
                mnt.vnc_quality = user_quality
                mnt = Mount.save(payload_dir, codename, exist_ok=True)
            except Exception as e:
                pass
        try:
            if vnc_proc is not None:
                vnc_proc.wait()
        finally:
            while proc is not None and proc.poll() is None:
                typer.echo(f"[+] Shutting down (please wait)...")
                proc.terminate()
                try:
                    proc.wait(10)
                except subprocess.TimeoutExpired:
                    pass
