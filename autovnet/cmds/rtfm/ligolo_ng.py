import os
import shlex
import shutil
from pathlib import Path
from typing import Optional, List
from threading import Event

import typer
import pyfiglet

from ...autovnet import AutoVnet, avn
from autovnet.utils import (
    compose_run,
    gen_codename,
    complete_codename,
    rand_port,
    find_rt_tables_path,
    tun_name,
    compose_up,
)

app = typer.Typer()

MOUNT_TYPE = "ligolo-ng"
CONTAINER_DATA_DIR = f"/opt/{MOUNT_TYPE}"
DEFAULT_FONT = "cyberlarge"
TUN_PREFIX = "lgng-"


@app.command()
def server(
    networks: Optional[List[str]] = typer.Option(
        None, "-n", "--network", help="Simulated network to draw a random IP from."
    ),
    count: Optional[int] = typer.Option(
        1, "-c", "--count", help="The number of remote IP addresses to allocate."
    ),
    payload_dir: Optional[str] = typer.Option(
        avn.data_path(MOUNT_TYPE),
        "-D",
        "--payload-dir",
        help="Directory to save command metadata.",
    ),
    dest_ip: Optional[str] = typer.Option(
        "127.0.0.1",
        "-d",
        "--dest-ip",
        help="Destination IP for tunnelled traffic (default: 127.0.0.1).",
    ),
    dest_port: Optional[int] = typer.Option(
        None,
        "-p",
        "--dest-port",
        help="Destination port for tunnelled traffic (default: random).",
    ),
    remote_port: Optional[int] = typer.Option(
        443,
        "-P",
        "--remote-port",
        help="Public autovnet port the target will connect to. (default: 443).",
    ),
    version: Optional[str] = typer.Option(
        "latest",
        "-V",
        "--version",
        help="ligolo-ng version to use. See https://github.com/nicocha30/ligolo-ng/releases",
    ),
    random_register: Optional[int] = typer.Option(
        0,
        "-R",
        "--register-random",
        count=True,
        help="Register a random DNS name (repeatable).",
    ),
    register: Optional[List[str]] = typer.Option(
        None,
        "-r",
        "--register",
        help="Register a DNS name (repeatable).",
    ),
    cert: Optional[str] = typer.Option(
        None,
        "--cert",
        help="TLS cert to use (PEM)",
    ),
    key: Optional[str] = typer.Option(
        None,
        "--key",
        help="TLS key to use (PEM)",
    ),
    codename: Optional[str] = typer.Argument(
        None,
        autocompletion=complete_codename,
        help="Previous codename to load",
    ),
):
    """Run a ligolo-ng proxy (server)

    Ligolo-ng is a simple, lightweight and fast tool that allows pentesters to establish tunnels from a reverse TCP/TLS connection using a tun interface (without the need of SOCKS).
    See https://github.com/nicocha30/ligolo-ng

    This command starts a ligolo-ng proxy (server), accessible via an autovnet listener.
    It also creates a local tun.
    After receiving a connection from your agent, you probably want to `start` the proxy
    and use `autonet rtfm ligolo-ng route` to add routes to your ligolo tun.
    Any routes added to the ligolo tun will route through the agent.

    Last tested version: v0.4.4
    """

    ctx = None
    new_svc = False

    loaded_previous = True
    if codename is None:
        codename = gen_codename()
        loaded_previous = False

    payload_dir = Path(payload_dir)
    ligolo_ng_dir = payload_dir / codename
    ligolo_ng_dir.mkdir(parents=True, exist_ok=True)
    certs_dir = ligolo_ng_dir / "certs"
    certs_dir.mkdir(parents=True, exist_ok=True)

    # Discover existing key / cert, if present
    if (certs_dir / "cert.pem").is_file():
        cert = str(certs_dir / "cert.pem")
    if (certs_dir / "key.pem").is_file():
        key = str(certs_dir / "key.pem")

    proxy_args = []

    # Path in container where stuff is mounted
    container_dir = Path(CONTAINER_DATA_DIR)

    if cert is None and key is None:
        proxy_args.append("-selfcert")
    else:
        if cert is None or key is None:
            raise ValueError("Provide both --cert and --key or neither")

        cert_path = certs_dir / "cert.pem"
        shutil.copyfile(cert, cert_path)
        proxy_args.extend(["-certfile", str(cert_path)])

        key_path = certs_dir / "key.pem"
        shutil.copyfile(key, key_path)
        proxy_args.extend(["-keyfile", str(key_path)])

    # Prepare compose
    docker_path = avn.docker_path(f"plugins/{MOUNT_TYPE}/")

    ln = None
    if loaded_previous:
        # hack: merge any saved listeners into a single mega-listener object
        models = []
        for path in Path(ligolo_ng_dir).glob("*.yaml"):
            ln = avn.listener_from_codename(
                codename, payload_dir=payload_dir, filename=path.name
            )
            models.extend(ln.lns)
        ln = avn.listener_from_models(models, codename=codename)
    else:

        if dest_port is None:
            dest_port = rand_port()

        ln = avn.listener(
            networks,
            [remote_port] * count,
            dest_ip,
            dest_port=dest_port,
            random_register=random_register,
            register=register[count:],
            codename=codename,
        )
        ln.save(payload_dir)

    # Start autovnet listeners
    with ln.listener():

        rt_tables_path = find_rt_tables_path()
        ligolo_ng_tun_name, ligolo_ng_tun_alias = tun_name(codename, prefix=TUN_PREFIX)

        iptables_project = f"lgng_ip_{codename}"
        try:
            ip_cmds = [
                # Make a tun
                [
                    "ip",
                    "tuntap",
                    "add",
                    "user",
                    str(os.geteuid()),
                    "mode",
                    "tun",
                    ligolo_ng_tun_name,
                ],
                # Mark it up
                ["ip", "link", "set", ligolo_ng_tun_name, "up"],
                # Add a cosmetic alias
                ["ip", "link", "set", ligolo_ng_tun_name, "alias", ligolo_ng_tun_alias],
            ]
            for ip_cmd in ip_cmds:
                env = {
                    "IPTABLES_CMD": shlex.join(ip_cmd),
                    "RT_TABLES_PATH": str(rt_tables_path),
                }
                iptables_docker_path = avn.docker_path(f"plugins/iptables/")
                if avn.verbose:
                    typer.echo(f"[~] {shlex.join(ip_cmd)}")

                compose_up(
                    iptables_docker_path,
                    env=env,
                    project=iptables_project,
                    exit_code_from="iptables",
                    quiet=not avn.verbose,
                )

            typer.echo("=" * 80)
            art = pyfiglet.figlet_format(MOUNT_TYPE, font=DEFAULT_FONT)
            art = "\n".join(line for line in art.splitlines() if line.strip())
            typer.echo(art)
            typer.echo("\n" + "=" * 80)
            typer.echo(f"[+] autovnet rtfm ligolo_ng {codename}")

            for model in ln.lns:

                agent_cmd_ip = [
                    "agent",
                    "-ignore-cert",
                    "-retry",
                    "-connect",
                    f"{model.remote_ip}:{model.remote_port}",
                ]

                typer.echo(f"[+] target: {shlex.join(agent_cmd_ip)}")

                names = []
                if model.dns_names is not None:
                    names = model.dns_names
                for dns in names:
                    agent_cmd_dns = [
                        "agent",
                        "-ignore-cert",
                        "-retry",
                        "-connect",
                        f"{dns}:{model.remote_port}",
                    ]
                    typer.echo(f"[+] {shlex.join(agent_cmd_dns)}")

            typer.echo(
                f"[+] route: autovnet rtfm ligolo_ng route {codename} X.X.X.X/YY"
            )
            typer.echo("=" * 80)
            typer.echo(f"[+] agents: {ligolo_ng_dir}/bin")
            typer.echo(f"[+] tun: {ligolo_ng_tun_name} / {ligolo_ng_tun_alias}")
            typer.echo("=" * 80)

            typer.echo("1. Start the server (this command)")
            typer.echo(
                "2. Run the agent on a target > get the session > `session` (pick session) > `start`"
            )
            typer.echo(
                "3. Route through agent (other terminal): autovnet rtfm ligolo_ng route X.X.X.X/YY"
            )
            typer.echo("=" * 80)

            project = f"{MOUNT_TYPE}_{codename}"

            # Get bind address from first listener
            dest_ip, dest_port = ln.lns[0].dest_ip, ln.lns[0].dest_port
            proxy_args.extend(
                [
                    "-laddr",
                    f"{dest_ip}:{dest_port}",
                    "-tun",
                    ligolo_ng_tun_name,
                ]
            )
            if avn.verbose:
                proxy_args.append("-v")
                typer.echo(f"./proxy {shlex.join(proxy_args)}")

            env = {
                "LIGOLO_NG_VERSION": version,
                "LIGOLO_NG_DATA": str(ligolo_ng_dir),
                "UID": str(os.geteuid()),
                "GID": str(os.getegid()),
                "LIGOLO_NG_PROXY_ARGS": shlex.join(proxy_args),
            }

            compose_run(
                docker_path,
                MOUNT_TYPE,
                env=env,
                project=project,
                build=True,
                verbose_build=avn.verbose,
            )
        finally:
            # Delete the tun, which should remove any associated routes
            ip_cmd = ["ip", "link", "del", ligolo_ng_tun_name]
            env = {
                "IPTABLES_CMD": shlex.join(ip_cmd),
                "RT_TABLES_PATH": str(rt_tables_path),
            }
            iptables_docker_path = avn.docker_path(f"plugins/iptables/")
            if avn.verbose:
                typer.echo(f"[~] {shlex.join(ip_cmd)}")
            compose_up(
                iptables_docker_path,
                env=env,
                project=iptables_project,
                exit_code_from="iptables",
                quiet=not avn.verbose,
            )


@app.command()
def route(
    payload_dir: Optional[str] = typer.Option(
        avn.data_path(MOUNT_TYPE),
        "-D",
        "--payload-dir",
        help="Directory to save command metadata.",
    ),
    delete: Optional[bool] = typer.Option(
        False,
        "-d",
        "--delete",
        help="Delete routes instead of adding them",
    ),
    codename: str = typer.Argument(
        None,
        autocompletion=complete_codename,
        help="Codename of running ligolo_ng server",
    ),
    routes: List[str] = typer.Argument(
        None,
        help="Routes to add to ligolo tunnel (repeatable).",
    ),
):
    """Attach routes to an active ligolo-ng proxy

    You *must* have:
        * an autovnet rtfm ligolo_ng server running
        * an active agent session
        * an active proxy (session > start in ligolo)

    WARNING: If you do not have an active proxy running, the route will be a black hole,
    and you will not be able to reach the route, even if you could previously

    Routes can be deleted with this command.
    When the autovnet rtfm ligolo_ng server stops (e.g. CTRL+C),
    any routes associated with the tun are automatically removed
    """

    payload_dir = Path(payload_dir)
    ligolo_ng_dir = payload_dir / codename
    if not ligolo_ng_dir.is_dir():
        raise FileNotFoundError(
            f"{ligolo_ng_dir} not found! Is {codename} a valid codename?"
        )

    # Our tunnel naming is determisitic based on the codename
    ligolo_ng_tun_name, ligolo_ng_tun_alias = tun_name(codename, prefix=TUN_PREFIX)

    mode = "add"
    if delete:
        mode = "del"

    iptables_docker_path = avn.docker_path(f"plugins/iptables/")
    iptables_project = f"lgng_route_{codename}"
    rt_tables_path = find_rt_tables_path()

    for r in routes:
        ip_cmd = ["ip", "route", mode, r, "dev", ligolo_ng_tun_name]
        env = {
            "IPTABLES_CMD": shlex.join(ip_cmd),
            "RT_TABLES_PATH": str(rt_tables_path),
        }
        if avn.verbose:
            typer.echo(f"[~] {shlex.join(ip_cmd)}")

        compose_up(
            iptables_docker_path,
            env=env,
            project=iptables_project,
            exit_code_from="iptables",
            quiet=not avn.verbose,
        )
        typer.echo(
            f"[+] {mode} route: {r} via dev {ligolo_ng_tun_name} ({ligolo_ng_tun_alias})"
        )

    if mode == "add":
        typer.echo(
            f"[+] delete routes: autovnet rtfm ligolo_ng route -d {codename} {shlex.join(routes)}"
        )
