from . import config
from . import net
from . import tmp
from . import docs
from . import rtfm
from . import apt
from . import pasta
from . import type
